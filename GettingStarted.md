_User manual_

**Hardware installation instructions:**

The hat must be mounted on to the 40 pin header socket on the Pi, by alligning the pins labelled as "Pin 1"

The 4 spacers should be placed on the repective corners of the boards and 
fastened to one another with the screws provided.

The battery can be connected using any type of insulated copper wire and 
the pads on the PCB marked as supply V+ and V-.

**Software installation instructions:**

Once the hardware assembled, the program (provided in this repo) that runs the UPS must be
installed on the Pi.

The installation files should open up a wizard with easy to follow instructions.

Omce the software is installed the the 3 LED indicators should light up consecutively, and blink twice.

**Testing the UPS hat:**

Before testing the UPS, make sure a battery with a voltage of 9 or above is 
connected, as well as the DC power supply for the PI

To test the UPS, switch off the DC power supply.

The LED indicators should turn on with respect to the voltage provided.

If the battery voltage is reasonably low the led should start flashing, on the Pi's gui a 
warning message should appear, prompting you to shut the PI down 

